##### PANDAS

<a href="https://gitlab.com/marcelorodriguesss/python_faq_coding/blob/master/pandas.ipynb" target="_blank">Como ler um arquivo Excel</a><br/>
<a href="https://gitlab.com/marcelorodriguesss/python_faq_coding/blob/master/pandas.ipynb" target="_blank">Como ler um arquivo Excel</a>


## REFERENCES

##### https://gist.github.com/bsweger/e5817488d161f37dcbd2
##### http://www.marcelscharth.com/python/time.html
##### https://jeffdelaney.me/blog/useful-snippets-in-pandas
##### http://wrf-python.readthedocs.io/en/latest/plot.html
##### http://xarray.pydata.org/en/stable/auto_gallery/plot_cartopy_facetgrid.html
##### http://queirozf.com/entries/pandas-dataframe-by-example
##### http://pandas.pydata.org/pandas-docs/stable/timeseries.html#offset-aliases
